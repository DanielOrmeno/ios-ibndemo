/*******************************************************************************************************************
|   File: SecondViewController.swift
|   Proyect: IBNdemo
|
|   Description: - TODO -
|
|
|   Created by: Daniel Ormeño
|   Created on: 20/10/2014
|
|   Copyright (c) 2014 Daniel Ormeño. All rights reserved.
*******************************************************************************************************************/

import UIKit
import CoreLocation

class SecondViewController: UIViewController {

    //-- Mutable UI elements
    @IBOutlet weak var beaconsLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //- Nav Bar Customization
        self.tabBarController?.tabBar.tintColor = UIColor.grayColor()
        self.tabBarController?.tabBar.barTintColor = UIColor.blackColor()
        self.tabBarController?.tabBar.translucent = true
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

