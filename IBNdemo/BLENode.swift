/*******************************************************************************************************************
|   File: BLENode.swift
|   Proyect: IBNdemo
|
|   Description: - BLENode, extention of Apples CLBeacon class. The original class from apple does not provide mutators
|   for the beacon objects, thus a new class must be provided for the creation of IBN nodes for BLE location
|
|
|   Created by: Daniel Ormeño
|   Created on: 17/11/2014
|
|   Copyright (c) 2014 Daniel Ormeño. All rights reserved.
*******************************************************************************************************************/

import CoreLocation

class BLENode {
    
    // =====================================     INSTANCE VARIABLES / PROPERTIES      =============================//
    
    //- FROM CLBEACON CLASS (No setters available on default class):
    var proximityUUID: NSUUID!
    var major: NSNumber!
    var minor: NSNumber!
    var rssi: Int
    
    //- Relative location properties
    var xAxis: Int
    var yAxis: Int
    var relativeDistance: Double
    
    //IBN beacon id
    var IBNid: String
    
    // =====================================     CLASS CONSTRUCTORS      =========================================//
    
    init (beacon: CLBeacon , x: Int, y:Int, id: String) {
        
        //- Beacon values
        self.proximityUUID = beacon.proximityUUID
        self.major = beacon.major
        self.minor = beacon.minor
        self.rssi = beacon.rssi
        
        self.IBNid = id
        
        self.xAxis = x
        self.yAxis = y
        
        //- Temporary value to avoid using optionals
        self.relativeDistance = 0.0
        self.relativeDistance = resolveRSSI(Double(self.rssi))
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // =====================================     CLASS METHODS      ===============================================//
    
    func resolveRSSI (rssi: Double) -> Double {
        
        //- Positioning Constants
        let YINTERCEPT = -68.932
        let COEFFICIENT = -4.228
        return exp((rssi - YINTERCEPT)/COEFFICIENT)
    }
} // END OF NODE CLASS