/*******************************************************************************************************************
|   File: FirstViewController.swift
|   Proyect: IBNdemo
|
|   Description: - View controller for the first tab (Locate me). Extends CBCentralManagerDelegate to listen to any
|                relevant events regarding changes to BLE hardware. It creates an instance of the class BLEScanner to
|                scan for beacons and retrieve relevant data.
|
|
|   Created by: Daniel Ormeño
|   Created on: 20/10/2014
|
|   Copyright (c) 2014 Daniel Ormeño. All rights reserved.
*******************************************************************************************************************/

import UIKit
import CoreLocation
import CoreBluetooth

class FirstViewController: UIViewController, CBCentralManagerDelegate, BLEScannerDelegate {
    
    // =====================================     View GUI Elements       ==========================================//
    //- Labels
    @IBOutlet weak var statusValueLabel: UILabel!
    @IBOutlet weak var beacon00Label: UILabel!
    @IBOutlet weak var beacon10Label: UILabel!
    @IBOutlet weak var beacon01Label: UILabel!
    @IBOutlet weak var beacon11Label: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var statsLabel: UILabel!
    
    //- Nested Views
    @IBOutlet weak var LocationArea: UIView!
    
    //- Location and beacon Icons
    
    @IBOutlet weak var locationIcon: UIView!
    @IBOutlet weak var beacon00Icon: UIImageView!
    @IBOutlet weak var beacon10Icon: UIImageView!
    @IBOutlet weak var beacon01Icon: UIImageView!
    @IBOutlet weak var beacon11Icon: UIImageView!
    
    // =====================================     INSTANCE VARIABLES / PROPERTIES      =============================//
    
    //- Beacon Scanner object to detect beacons in proximity based on known regions
    var beaconScanner = BLEScanner()
    
    //- CB Central Manager - Check's Bluetooth
    var centralManager : CBCentralManager = CBCentralManager()
    
    //- Data Structures - Scanned Beacons
    var scannedBeacons = [CLBeacon]()
    
    //- Array of Regions based on valid UUIDs - BLEScannerProtocol requisits
    var beaconRegions = [CLBeaconRegion]()
    var proximityBLENodes = [BLENode]()
    
    //var relativeLocation : (Double, Double)?
    
    //- Last known rssi values for each node
    var lastKnownRssi: (rssi00: Int, rssi10: Int, rssi01: Int, rssi11: Int) = (-60,-60,-60,-60)
    
    //- Screen size of device
    let screenSize: CGRect = UIScreen.mainScreen().bounds
    
    // =====================================     VIEW CONTROLLER REQUIRED METHODS      ===========================//
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        //- Nav Bar Customization
        self.tabBarController?.tabBar.tintColor = UIColor.grayColor()
        self.tabBarController?.tabBar.barTintColor = UIColor.blackColor()
        self.tabBarController?.tabBar.translucent = true
        
        //- Hides all beacon icons
        self.beacon00Icon.hidden = true
        self.beacon01Icon.hidden = true
        self.beacon10Icon.hidden = true
        self.beacon11Icon.hidden = true
        
        //- Bluetooth Core Manager
        centralManager = CBCentralManager(delegate: self, queue: nil)
        
        //- Setup of ble regions to be scanned
        beaconRegions  = setupOfValidRegions()
        
        //- Instantiation of BLEScanner object and delegate
        beaconScanner = BLEScanner(regions: beaconRegions)
        beaconScanner.delegate = self
        
        //- Starts Ranging for valid regions
        beaconScanner.scanForBeaconsInProximity()
        
        //- NSTimmer for estimating location every 0.5 seconds
        var timer = NSTimer.scheduledTimerWithTimeInterval(0.5, target: self, selector: Selector("calculateLocation"), userInfo: nil, repeats: true)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // =====================================    CLASS METHODS   ==================================================//
    
    /********************************************************************************************************************
    METHOD NAME: setupRegions
    INPUT PARAMETERS: None
    RETURNS: CLBeaconRegion array
    
    OBSERVATIONS: This method sets up all the valid regions to be scanned, this needs to be later changed so that the
    values are no longer hardcoded but fetched from a remote server.
    
    _ TODO: CONSIDER INCLUDING THIS FUNCTIONALITY TO THE BLEScanner CLASS WHEN INCLUDING SERVER CALLS _
    ********************************************************************************************************************/
    func setupOfValidRegions () -> [CLBeaconRegion] {
        
        //- IBN MiniBeacon UUIDs
        let MiniBeaconUUID000 = "E2C56DB5-DFFB-48D2-B060-D0F5A71096E0"
        let MiniBeaconUUID001 = "A39A0BA7-3254-4CCA-8155-62137FFA0E52"
        let MiniBeaconUUID002 = "5D763BB9-C43F-40BE-A6CD-8102B8C69A72"
        let MiniBeaconUUID003 = "9198F214-9EAB-42EA-BF4D-2182416D8FC9"
        let MiniBeaconUUID004 = "80FE1B8A-729A-4AA1-A567-06AB41312F1D"
        let MiniBeaconUUID005 = "19D42F3B-A6E0-4C1A-84EF-A49852B3C840"
        
        /*//- IBN microBeacon UUIDS
        let MicroBeaconUUID1 = "EBEFD083-70A2-47C8-9837-E7B5634DF001"
        let MicroBeaconUUID2 = "EBEFD083-70A2-47C8-9837-E7B5634DF002"
        let MicroBeaconUUID3 = "EBEFD083-70A2-47C8-9837-E7B5634DF003"
        let MicroBeaconUUID4 = "EBEFD083-70A2-47C8-9837-E7B5634DF004"
        
        //- Emulate Beacon iOS UUID
        let iOSVirtualBeaconUUID = "0CF052C2-97CA-407C-84F8-B62AAC4E9020"*/
        
        //- Beacons utilized for Gold Coast Hospital DEMO
        let MiniBeacon000  = (MiniBeaconUUID000, "miniBeacon_000")
        let MiniBeacon001  = (MiniBeaconUUID001, "miniBeacon_001")
        let MiniBeacon002  = (MiniBeaconUUID002, "miniBeacon_002")
        let MiniBeacon003  = (MiniBeaconUUID003, "miniBeacon_003")
        let MiniBeacon004  = (MiniBeaconUUID004, "miniBeacon_004")
        let MiniBeacon005  = (MiniBeaconUUID005, "miniBeacon_005")
        /*
        let MicroBeacon001 = (MicroBeaconUUID1, "microBeacon001")
        let MicroBeacon002 = (MicroBeaconUUID2, "microBeacon002")
        let MicroBeacon003 = (MicroBeaconUUID3, "microBeacon003")
        let MicroBeacon004 = (MicroBeaconUUID4, "microBeacon004")
        
        let iOSVirtualBeacon = (iOSVirtualBeaconUUID, "iOSVirtualBeacon")*/
        
        //- Array of tuples - Each tuple is a beacon (UUID, Indetifier)
        var validBeacons = [(String, String)]()
        
        //- Creation of the array of valid UUIDs
        //validBeacons.append(MiniBeacon000)
        validBeacons.append(MiniBeacon001)
        validBeacons.append(MiniBeacon002)
        validBeacons.append(MiniBeacon003)
        validBeacons.append(MiniBeacon004)
        /*validBeacons.append(MiniBeacon005)
        validBeacons.append(MicroBeacon001)
        validBeacons.append(MicroBeacon002)
        validBeacons.append(MicroBeacon003)
        validBeacons.append(MicroBeacon004)
        validBeacons.append(iOSVirtualBeacon)*/
        
        //- Creation of Regions for ranging - One Region for each known beacon
        // ** TODO: ** CREATE A STANDARD METHOD FOR LOCATING BEACONS IN WARDS, REUSE UUIDs
        for beacon in validBeacons {
            beaconRegions.append(CLBeaconRegion (proximityUUID: NSUUID(UUIDString: beacon.0), identifier: beacon.1))
        }
        return beaconRegions
    }
    
    
    /********************************************************************************************************************
    METHOD NAME: updateLocationIcon
    INPUT PARAMETERS: xAxis and yAxis coordenates of type Double
    RETURNS: N/A
    
    OBSERVATIONS: This method is called each time a new location value is calculated. It updates the position of the 
                  location icon in the view
    ********************************************************************************************************************/
    func updateLocationIcon(xAxis: Double, yAxis: Double){
        
        //self.locationIcon.frame.origin.x = 125
        //self.locationIcon.frame.origin.y = 125
        
        //- Adjusts the x and y values to the size of the view (300 px)
        let calibratedX = xAxis*300
        let calibratedY = yAxis*300
        
        let endPoint = CGPoint(x: calibratedX, y: calibratedY)
        
        UIView.animateWithDuration(0.5, delay:0, options: .CurveEaseOut, animations: {
            
            //self.locationIcon.frame.origin.x += (endPoint.x-125)
            //self.locationIcon.frame.origin.y += (endPoint.y-125)
            self.locationIcon.frame.origin.x = endPoint.x
            self.locationIcon.frame.origin.y = endPoint.y
            
            }, completion: { finished in })
        
        let xlabel = NSString(format:"%.2f", xAxis)
        let ylabel = NSString(format:"%.2f", yAxis)
        
        self.locationLabel.text = " x=\(xlabel), y=\(ylabel)"
    }
    
    /********************************************************************************************************************
    METHOD NAME: updateBeaconIcons
    INPUT PARAMETERS: Array of scanned BLENodes
    RETURNS: N/A
    
    OBSERVATIONS: Hides or shows the beacon Icons in the UI to show the user which beacons are being scanned
    ********************************************************************************************************************/
    func updateUIElements (nodes: [BLENode]){
        
        var b00:Bool = true
        var b10:Bool = true
        var b01:Bool = true
        var b11:Bool = true
        
        for eachNode in nodes {
            
            switch eachNode.IBNid {
                
            case "miniBeacon_001":
                if self.beacon00Icon.hidden {
                    self.beacon00Icon.hidden = false
                }
                self.beacon00Label.text = String(eachNode.rssi)
                self.lastKnownRssi.rssi00=eachNode.rssi
                b00 = false
            case "miniBeacon_002":
                if self.beacon10Icon.hidden {
                    self.beacon10Icon.hidden = false
                }
                self.beacon10Label.text = String(eachNode.rssi)
                self.lastKnownRssi.rssi10=eachNode.rssi
                b10 = false
            case "miniBeacon_003":
                if self.beacon01Icon.hidden {
                    self.beacon01Icon.hidden = false
                }
                self.beacon01Label.text = String(eachNode.rssi)
                self.lastKnownRssi.rssi01=eachNode.rssi
                b01 = false
            case "miniBeacon_004":
                if self.beacon11Icon.hidden {
                    self.beacon11Icon.hidden = false
                }
                self.beacon11Label.text = String(eachNode.rssi)
                self.lastKnownRssi.rssi11=eachNode.rssi
                b11 = false
            default:
                println("error, invalid node scanned")
            }
        }
        
        //- Hides Icons of beacons that are no longer in range
        if b00 {
            self.beacon00Icon.hidden = true
            self.beacon00Label.text = ""
        }
        
        if b10 {
            self.beacon10Icon.hidden = true
            self.beacon10Label.text = ""
        }
        
        if b01 {
            self.beacon01Icon.hidden = true
            self.beacon01Label.text = ""
        }
        
        if b11 {
            self.beacon11Icon.hidden = true
            self.beacon11Label.text = ""
        }
        
        //- Updates number of beacons in range
        self.statusValueLabel.text = String(nodes.count)
    }
    
    // =====================================    BLEScannerDelegate METHODS   ==================================//
    
    func didScanForBeaconsInProximity (newBeacons: [CLBeacon], fromRegion: CLBeaconRegion!){
        
        //-- Updates array of beacons in range
        for eachBeacon in newBeacons {
            
            if (foundNewBeacon(eachBeacon)){
                scannedBeacons.append(eachBeacon)
            }
            
            //- UNCOMMENT FOR DATA STRACTION AND ANALYSIS
            //println("\(fromRegion.identifier):_UUID:\(eachBeacon.proximityUUID.UUIDString)_RSSI:\(eachBeacon.rssi)_proximity:\(eachBeacon.proximity.rawValue)_accuracy:\(eachBeacon.accuracy)")
            //println("\(fromRegion.identifier):,\(eachBeacon.proximityUUID.UUIDString),\(eachBeacon.rssi)")
        }
    }
    
    func foundNewBeacon (beacon: CLBeacon) -> Bool {
        
        var foundBeacon = true
        
        for eachBeacon in self.scannedBeacons {
            if (beacon.proximityUUID == eachBeacon.proximityUUID) && (beacon.major == eachBeacon.major) && (beacon.minor == eachBeacon.minor) {
                foundBeacon = false
            }
        }
        return foundBeacon
    }
    
    func convertBeaconToBLENode( beacon: CLBeacon) -> BLENode {
        
        var coordinates : (Int, Int)
        var idValue:String
        
        switch beacon.proximityUUID.UUIDString {
            
        case "E2C56DB5-DFFB-48D2-B060-D0F5A71096E0":
            idValue = "miniBeacon_000"
            coordinates = (0,0)
        case "A39A0BA7-3254-4CCA-8155-62137FFA0E52":
            idValue = "miniBeacon_001"
            coordinates = (0,0)
        case "5D763BB9-C43F-40BE-A6CD-8102B8C69A72":
            idValue = "miniBeacon_002"
            coordinates = (1,0)
        case "9198F214-9EAB-42EA-BF4D-2182416D8FC9":
            idValue = "miniBeacon_003"
            coordinates = (0,1)
        case "80FE1B8A-729A-4AA1-A567-06AB41312F1D":
            idValue = "miniBeacon_004"
            coordinates = (1,1)
        case "19D42F3B-A6E0-4C1A-84EF-A49852B3C840":
            idValue = "miniBeacon_005"
            coordinates = (0,0)
        default:
            idValue = "Nil"
            coordinates = (-0,-0)
        }
        
        return BLENode(beacon: beacon, x: coordinates.0, y: coordinates.1, id: idValue)
        
    }
    
    func calculateLocation() -> Bool {
        
        //- Clear last location nodes
        self.proximityBLENodes.removeAll(keepCapacity: false)
        
        //- Calculates location if more than one beacon is in proximity, then clears the array for next scan.
        if self.scannedBeacons.count > 0 {
            
            //- Converts beacons to BLENodes and appends nodes to array of newly scanned beacons
            for eachBeacon in self.scannedBeacons {
                self.proximityBLENodes.append(convertBeaconToBLENode(eachBeacon))
            }
            
            //- Change rssi values 0 to last known rssi
            for eachNode in self.proximityBLENodes {
                
                if eachNode.rssi>=0{
                    switch eachNode.IBNid {
                        
                    case "miniBeacon_001":
                        eachNode.rssi = self.lastKnownRssi.rssi00
                    case "miniBeacon_002":
                        eachNode.rssi = self.lastKnownRssi.rssi10
                    case "miniBeacon_003":
                        eachNode.rssi = self.lastKnownRssi.rssi01
                    case "miniBeacon_004":
                        eachNode.rssi = self.lastKnownRssi.rssi11
                    default:
                        println("error, invalid node with rssi=0")
                    }
                    
                }
            }
            
            //- Update text and Icons in UI
            updateUIElements (proximityBLENodes)
            
            //- Minimun number of beacons in range to calculate the location
            if self.proximityBLENodes.count>2 {
                //- Estimates location based on newly scanned nodes
                beaconScanner.getLocation(proximityBLENodes)
            }
            
            //- Clears scannedBeacons array
            self.scannedBeacons.removeAll(keepCapacity: false)
        }
        
        //- Optional chaining for optional location value
        if let location = beaconScanner.location {
            updateLocationIcon(location.0, yAxis: location.1)
            //- Reset Location Value
            beaconScanner.location = nil
            return true
        } else {
            return false
        }
    }
    
    // =====================================    CBCentralManagerDelagate METHODS   ===============================//
    
    func centralManagerDidUpdateState (central: CBCentralManager!){
        
        switch central.state {
            
        case .PoweredOff:
            println("CoreBluetooth BLE hardware is powered off")
            self.statsLabel.text = "Bluetooth hardware is off"
            self.locationLabel.text = "unknown"
            self.statusValueLabel.text = "none"
            break
        case .PoweredOn:
            println("CoreBluetooth BLE hardware is powered on and ready")
            self.statsLabel.text = "Scanning for beacons..."
            break
        case .Resetting:
            println("CoreBluetooth BLE hardware is resetting")
            self.statsLabel.text = "Reseting Bluetooth hardware"
            self.locationLabel.text = "unknown"
            self.statusValueLabel.text = "none"
            break
        case .Unauthorized:
            println("CoreBluetooth BLE state is unauthorized")
            break
        case .Unknown:
            println("CoreBluetooth BLE state is unknown")
            break
        case .Unsupported:
            println("CoreBluetooth BLE hardware is unsupported on this platform")
            self.statsLabel.text = "Bluetooth is not supported"
            break
        default:
            println("centralManagerDidUpdateState switch default clause")
            break
        }
    }
}

