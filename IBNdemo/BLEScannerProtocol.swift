/*******************************************************************************************************************
|   File: BLEScannerProtocol.swift
|   Proyect: IBNdemo
|
|   Description: - Protocol for scanning and relative location estimation based on the IBN trilateration algorithm.
|
|
|   Created by: Daniel Ormeño
|   Created on: 28/10/2014
|
|   Copyright (c) 2014 Daniel Ormeño. All rights reserved.
*******************************************************************************************************************/

import Foundation
import CoreLocation

protocol BLEScannerDelegate {
    
    // =====================================     PROPERTY REQUIREMENTS    =========================================//
    
    var scannedBeacons    : [CLBeacon]   {get set}
    var proximityBLENodes : [BLENode]    {get set}
    
    // =====================================     METHOD REQUIREMENTS      ========================================//
    
    /********************************************************************************************************************
    METHOD NAME: didScanForBeacons
    INPUT PARAMETERS: newBeacons: CLBeacons scanned for region, fromRegion: CLBeaconRegion scanned by calling class
    RETURNS: N/A
    
    OBSERVATIONS: This method is called when the BLEScanner class detects a change of the RSSI of a beacon or group of 
                  beacons that conform a region
    
    ********************************************************************************************************************/
    func didScanForBeaconsInProximity (newBeacons: [CLBeacon], fromRegion: CLBeaconRegion!)
    
    /********************************************************************************************************************
    METHOD NAME: foundNewBeacon
    INPUT PARAMETERS: CLBeacon - Scanned beacon
    RETURNS: Bool
    
    OBSERVATIONS: This method checks if the scanned beacon is already in the scannedBeacons array of the class.
    
    ********************************************************************************************************************/
    func foundNewBeacon (beacon: CLBeacon) -> Bool
    
    /********************************************************************************************************************
    METHOD NAME: convertBeaconToBLENode
    INPUT PARAMETERS: CLBEacon
    RETURNS: BLENode
    
    OBSERVATIONS: Converts CLBeacon object (Apple) to IBN BLENode for location estimation, IBN rules need to be defined
                  in this method, these rules include: beacon identifier (ex. miniBeacon_001), x and y coordenates, etc.
                  based on scanned beacon proximity UUID and major and minor if applicable
    
    ********************************************************************************************************************/
    func convertBeaconToBLENode( beacon: CLBeacon) -> BLENode
    
    /********************************************************************************************************************
    METHOD NAME: calculateLocation
    INPUT PARAMETERS: None
    RETURNS: Bool
    
    OBSERVATIONS: Returns true if location can be calculated based on number of scanned beacons
    
    ********************************************************************************************************************/
    func calculateLocation () -> Bool
}
