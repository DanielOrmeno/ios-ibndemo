/*******************************************************************************************************************
|   File: BeaconScanner.swift
|   Proyect: IBNdemo
|
|   Description: - TODO -
|
|
|   Created by: Daniel Ormeño
|   Created on: 21/10/2014
|
|   Copyright (c) 2014 Daniel Ormeño. All rights reserved.
*******************************************************************************************************************/

import CoreFoundation
import CoreBluetooth
import CoreLocation
import CoreData
import UIKit


class BeaconScanner: NSObject, CBCentralManagerDelegate, CLLocationManagerDelegate {
    
    // =====================================     INSTANCE VARIABLES / PROPERTIES      ======================================//
    
    // Managers - Location, bleDevices, CBCentral
    var cManager: CBCentralManager
    var locationManager : CLLocationManager
    
    //- Valid UUIDS for raging
    let MiniBeaconUUID        = "E2C56DB5-DFFB-48D2-B060-D0F5A71096E0"
    let MicroBeaconUUID       = "EBEFD083-70A2-47C8-9837-E7B5634DF001"
    let IpadVirtualBeaconUUID = "0CF052C2-97CA-407C-84F8-B62AAC4E9020"
    
    //- Array of Valid UUIDs
    var validUUIDs = [CBUUID]()
    
    // =====================================     CLASS CONSTRUCTORS      ==================================================//
    
    override init(){
        
        // - Initialization of non-optional properties
        
        self.cManager = CBCentralManager()
        self.validUUIDs.append(CBUUID(string: MiniBeaconUUID))
        self.validUUIDs.append(CBUUID(string: MicroBeaconUUID))
        self.validUUIDs.append(CBUUID(string: IpadVirtualBeaconUUID))
        
        locationManager = CLLocationManager()
        
        // - Initialization of super class
        super.init()
        
        // - Configuration of Managers
        self.cManager = CBCentralManager(delegate: self, queue: nil)
        self.locationManager.delegate = self
        
        //- Authorization for utilization of location services
        if (CLLocationManager.authorizationStatus() != CLAuthorizationStatus.AuthorizedWhenInUse) {
            self.locationManager.requestWhenInUseAuthorization()
        }
        
    } //END-OF: class Constructor
    
    // =====================================     PROTOCOL COMPLIANCE SPECIFIC METHODS      =============================//
    
    func centralManagerDidUpdateState (cManager: CBCentralManager!){
        
        switch cManager.state {
            
        case .PoweredOff:
            println("CoreBluetooth BLE hardware is powered off")
            break
        case .PoweredOn:
            println("CoreBluetooth BLE hardware is powered on and ready")
            //Sarts Sacnning for BLE peripherals
            cManager.scanForPeripheralsWithServices(validUUIDs, options: nil)
            break
        case .Resetting:
            println("CoreBluetooth BLE hardware is resetting")
            break
        case .Unauthorized:
            println("CoreBluetooth BLE state is unauthorized")
            break
        case .Unknown:
            println("CoreBluetooth BLE state is unknown")
            break
        case .Unsupported:
            println("CoreBluetooth BLE hardware is unsupported on this platform")
            break
        default:
            println("centralManagerDidUpdateState switch default clause")
            break
        }
    }
    
    // =====================================     Core Bluetooth Central Manager DELEGATE METHODS   ====================//
    
    /*
    *       If a beacon is discovered in proximity, an optional CBperipheral reference to the beacon is created under de name foundBeacon.
    *       the characteristics of this device can be accessed through the CBperipheral methods.
    */
    
    func centralManager (central: CBCentralManager!, didDiscoverPeripheral peripheral: CBPeripheral!, advertisementData: [NSObject: AnyObject!]!, RSSI: NSNumber!) {
        
        println("DEBUG: Delegate method is called")
        
        // Hardware beacon
        println("PERIPHERAL NAME: \(peripheral.name)\n AdvertisementData: \(advertisementData)\n RSSI: \(RSSI)\n")
        
        println("UUID DESCRIPTION: \(peripheral.identifier.UUIDString)\n")
        
        println("IDENTIFIER: \(peripheral.identifier)\n")
        
        // stop scanning, saves battery power
        cManager.stopScan()
    }
    
    func centralManager(central: CBCentralManager!, didDisconnectPeripheral peripheral: CBPeripheral!, error: NSError!) {
        println("Error \(error)")
    }
    
    /*
    *       This method is called if a connection to the found beacon is established.
    *
    */
    
    func centralManager (central: CBCentralManager!, didConnectPeripheral peripheral: CBPeripheral!) {
        peripheral.discoverServices(nil)
        println("Connected to Beacon")
    }
    
    /*
    *       This method is called if a connection to the found beacon is refused
    *       The error is reported to the console output.
    */
    
    func centralManager(central: CBCentralManager!, didFailToConnectPeripheral peripheral: CBPeripheral!, error: NSError){
        println("Error: Failed to connect /(error)")
    }
    
    
}

