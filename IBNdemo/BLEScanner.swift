/*******************************************************************************************************************
|   File: BLEScanner.swift
|   Proyect: IBNdemo
|
|   Description: - BLEScanner Class - This class implements the Core Location to serve as a beacon scanner based on
|                known Regions, defined by the UUIDs of the beacons.
|                The beaconRegions:[CLBeaconRegion] is defined to store the known regions specified by validBeacons,
|                which are hardcoded tuples:(String,String) for UUIDs and region identifier value pairs.
|                The locationManager:CLLocationManager performs the scanning of beacons based on the above mentioned
|                regions and notifies its delegate (Itself) about any relevant events.
|
|
|   Created by: Daniel Ormeño
|   Created on: 25/10/2014
|
|   Copyright (c) 2014 Daniel Ormeño. All rights reserved.
*******************************************************************************************************************/

import Foundation
import CoreLocation
import UIKit
import Darwin

class BLEScanner : NSObject, CLLocationManagerDelegate {
    
    // =====================================     INSTANCE VARIABLES / PROPERTIES      =============================//
    
    //- Array of Regions based on valid UUIDs
    var beaconRegions : [CLBeaconRegion]?
    
    //- LocationManager for ranging
    var locationManager = CLLocationManager()
    
    //- IBN Nodes (beacons)
    var nodes = [BLENode]()
    
    //- array of X and Y midPoint coordinates - Resulting of the calculation of the midPoint of signal overlap areas
    var MidPointCoordinates = [(Double,Double)]()
    
    //- Current location
    var location: (Double, Double)?
    
    //- Delegate
    var delegate :BLEScannerDelegate?
    
    
    // =====================================     CLASS CONSTRUCTORS      ==========================================//
    
    override init () {
        
        super.init()
        
        //- Authorization for utilization of location services
        if (CLLocationManager.authorizationStatus() != CLAuthorizationStatus.AuthorizedWhenInUse) {
            self.locationManager.requestWhenInUseAuthorization()
        }
        
        locationManager.delegate = self
    }
    
    // -------------------------------------
    
    init (regions : [CLBeaconRegion]){
        
        self.beaconRegions = regions
        
        //- Super class constructor - NSObject
        super.init()
        
        //- Authorization for utilization of location services
        if (CLLocationManager.authorizationStatus() != CLAuthorizationStatus.AuthorizedWhenInUse) {
            self.locationManager.requestWhenInUseAuthorization()
        }
        
        //- Delegate for Location Related Events
        locationManager.delegate = self
        
    } //END-OF: class Constructor
    
    // =====================================     Core Location Manager DELEGATE METHODS   ==============================//
    
    func locationManager(manager: CLLocationManager!, didRangeBeacons beacons: [CLBeacon]!, inRegion region: CLBeaconRegion!) {
        
        //- Returns beacons in range to delegate object.
        self.delegate?.didScanForBeaconsInProximity (beacons, fromRegion: region)
    }
    
    // =====================================     CLASS METHODS   ======================================================//
    
    /********************************************************************************************************************
    METHOD NAME: scanForBeaconsInProximity
    INPUT PARAMETERS: None
    RETURNS: None
    
    OBSERVATIONS: Starts scanning beacons based on known proximityUUIDs passed onto the object upon initialization, if the
    object has now known valid regions, prints error message to console
    ********************************************************************************************************************/
    
    func scanForBeaconsInProximity () {
        
        //- Starts Scanning Beacons for each known region
        if let validRegions = beaconRegions {
            for region in validRegions {
                self.locationManager.startRangingBeaconsInRegion(region)
            }
        } else {println("Can't Scan, no valid regions known")}
    }
    
    /********************************************************************************************************************
    METHOD NAME: stopScanningForBeaconsInProximity
    INPUT PARAMETERS: None
    RETURNS: None
    
    OBSERVATIONS: Stops scanning beacons on all known proximityUUIDs passed onto the object, if the ject has now known
    valid regions, prints error message to console
    ********************************************************************************************************************/
    
    func stopScanningForBeaconsInProximity () {
        
        //- Starts Scanning Beacons for each known region
        if let validRegions = beaconRegions {
            for region in validRegions {
                self.locationManager.stopRangingBeaconsInRegion(region)
            }
        }
    }
    
    /********************************************************************************************************************
    METHOD NAME: resetLocationValues
    INPUT PARAMETERS: None
    RETURNS: None
    
    OBSERVATIONS: Resets all "last known" location values. Invoked by the getLocation method.
    ********************************************************************************************************************/
    func resetLocationValues () {
        
        //- Resets last location value
        self.location = nil
        
        //- Reset non-optional properties
        if !(self.nodes.isEmpty) {
            self.nodes.removeAll(keepCapacity: false)
        }
        
        if !(self.MidPointCoordinates.isEmpty) {
            self.MidPointCoordinates.removeAll(keepCapacity: false)
        }
    }
    
    /********************************************************************************************************************
    METHOD NAME: getLocation
    INPUT PARAMETERS: Array of BLENodes
    RETURNS: X,Y coordenates as a tuple (Double, Double)
    
    OBSERVATIONS:        TODO
    ********************************************************************************************************************/
    func getLocation (nodesInProximity: [BLENode]) -> (Double, Double) {
        
        //- Reset current values of location
        resetLocationValues()
        
        /*// Console output for testing - delete bellow
        
        for eachNode in nodesInProximity {
            switch eachNode.IBNid {
                
            case "miniBeacon_001":
                println("(0,0), rssi:\(eachNode.rssi)")
            case "miniBeacon_002":
                println("(1,0), rssi:\(eachNode.rssi)")
            case "miniBeacon_003":
                println("(0,1), rssi:\(eachNode.rssi)")
            case "miniBeacon_004":
                println("(1,1), rssi:\(eachNode.rssi)")
            default:
                println("error, invalid node scanned")
            }
        }
        //-- delete above*/
        
        //- array to be mutated
        self.nodes = nodesInProximity
        
        var newNodes = self.nodes
        
        var tempNode : BLENode
        var index = 0
        
        //- Appends pairs of nodes into pairedNodes array
        for node in newNodes {
            
            tempNode = node
            newNodes.removeAtIndex(index)
            
            for nextNode in newNodes {
                let tempTuple = getMidPoint(tempNode, nodeB: nextNode)
                MidPointCoordinates.append((tempTuple.0,tempTuple.1))
            }
        }
        
        var xPosition = [Double]()
        var yPosition = [Double]()
        
        for pair in MidPointCoordinates {
            xPosition.append(pair.0)
            yPosition.append(pair.1)
        }
        
        xPosition.sort {$0.0<$1.0}
        yPosition.sort {$0.0<$1.0}
        
        //- Median of arrays
        location = (xPosition[xPosition.count/2],yPosition[yPosition.count/2])
        
        MidPointCoordinates.removeAll(keepCapacity: false)
        
        //- return median value of each midPoint
        return location!
    }

    /********************************************************************************************************************
    METHOD NAME: getMidPoint
    INPUT PARAMETERS: Reference Node (nodeA) and paired node (nodeB)
    RETURNS: X,Y mid point coordenates as a tuple (Double, Double)
    
    OBSERVATIONS:
    ********************************************************************************************************************/
    func getMidPoint (nodeA: BLENode, nodeB: BLENode) -> (Double,Double) {
        
        //- Formula: TotalDistance = sqrt((Ax-Bx)^2 + (Ay - By)^2)
        
        // - delta X and delta Y
        let DeltaX = abs(nodeA.xAxis - nodeB.xAxis)
        let DeltaY = abs(nodeA.yAxis - nodeB.yAxis)
        
        //- TotalDistance between NodeA and NodeB as formula above
        let TotalDistance = sqrt(Double(DeltaX*DeltaX + DeltaY*DeltaY))
        
        //- Calculates the length of the signal overlap area:
        let overlap : Double = (nodeA.relativeDistance + nodeB.relativeDistance) - TotalDistance
        
        //- Calculates length from reference node to midpoint of signal overlap area
        let midPoint = nodeA.relativeDistance - (overlap/2)
        
        //- Calculates angle of actual midPoint
        let bearing = getBearing(nodeA, nodeB: nodeB)
        
        //- Get location of midPoint
        var xMidPoint = Double(nodeA.xAxis) + midPoint*cos(bearing)
        var yMidPoint = Double(nodeA.yAxis) + midPoint*sin(bearing)
        
        //println("nodeA:(\(nodeA.xAxis),\(nodeA.yAxis)),nodeB:(\(nodeB.xAxis),\(nodeB.yAxis)),overlap:\(overlap),midPoint:(\(xMidPoint),\(yMidPoint))")
        
        return (xMidPoint, yMidPoint)
    }
    
    /********************************************************************************************************************
    METHOD NAME: getBearing
    INPUT PARAMETERS: Reference Node (nodeA) and paired node (nodeB)
    RETURNS: X,Y mid point coordenates as a tuple (Double, Double)
    
    OBSERVATIONS: Calculates aproximate angle between reference node and midpoint (in radians)
    ********************************************************************************************************************/
    func getBearing (nodeA: BLENode, nodeB: BLENode) -> Double {
        
        var y = Double (nodeB.yAxis - nodeA.yAxis)
        var x = Double (nodeB.xAxis - nodeA.xAxis)
        
        //return RadiansToDegrees(atan2(y,x))
        return atan2(y,x)
    }
    
    /********************************************************************************************************************
    METHOD NAME: RadiansToDegrees
    INPUT PARAMETERS: radians of type Double
    RETURNS: convertion to degrees of type Double
    
    OBSERVATIONS: N/A
    ********************************************************************************************************************/
    func RadiansToDegrees (value:Double) -> Double {
        return value * 180.0 / M_PI
    }
}